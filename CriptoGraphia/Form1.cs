﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace CriptoGraphia
{
    public partial class Form1 : Form
    {
        string str_File ="";
        string alRus = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя 0123456789";
        string alEng = "abcdefghijklmnopqrstuvwxyz";
        bool f;
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }



        private void open_Click(object sender, EventArgs e)
        {


            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {



                str_File = File.ReadAllText(openFileDialog1.FileName);

                textBox2.Text = str_File;
            }
        }

        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }





        private void button1_Click(object sender, EventArgs e)
        {
            f = true;

            string lang = "eng";

            foreach (char c in textBox1.Text)
            {
                if ((c > 'а' && c < 'я') || (c > 'А' && c < 'Я'))
                    lang = "ru";
                else if ((c > 'a' && c < 'z') || (c > 'A' && c < 'Z'))
                    lang = "eng";
            }


            // textBox2.Text = Cezar(textBox1.Text, textBox3.Text, f);

            //шифровать


            if (str_File=="")

            {

            


                    if (comboBox1.SelectedIndex == 0)
                    {
                        textBox2.Text = Cezar(textBox1.Text, textBox3.Text, f);

                    }


                    if (comboBox1.SelectedIndex == 1)
                    {
                        textBox2.Text = Encipher(textBox1.Text, textBox3.Text, lang); ;

                    }

                if (comboBox1.SelectedIndex == 2)
                    {
                        textBox2.Text = PlayfairEncryption(textBox1.Text.ToLower(), textBox3.Text.ToLower(), lang);
                    }

                // textBox2.Text = File.ReadAllText(openFileDialog1.FileName);

            }else

            {
                foreach (char c in str_File)
                {
                    if ((c > 'а' && c < 'я') || (c > 'А' && c < 'Я'))
                        lang = "ru";
                    else if ((c > 'a' && c < 'z') || (c > 'A' && c < 'Z'))
                        lang = "eng";
                }


                

                if (comboBox1.SelectedIndex == 0)
                {
                    textBox2.Text = Cezar(str_File, textBox3.Text, f);

                }

                if (comboBox1.SelectedIndex == 1)
                {
                    textBox2.Text = Encipher(str_File, textBox3.Text, lang); ;

                }



                if (comboBox1.SelectedIndex == 2)
                {
                    textBox2.Text = PlayfairEncryption(str_File.ToLower(), textBox3.Text.ToLower(), lang);
                }
            }


        }















        public string Cezar(string text, string key, Boolean f)
        {
            string al = alRus;



            foreach (char c in text)
            {
                if ((c > 'а' && c < 'я') || (c > 'А' && c < 'Я'))
                    al = alRus;
                else if ((c > 'a' && c < 'z') || (c > 'A' && c < 'Z'))
                    al = alEng;
            }

            StringBuilder code = new StringBuilder();
            string s = text;
            string sd = textBox3.Text;


            int step = Convert.ToInt32(sd);

            for (int i = 0; i < s.Length; i++)
            {
                for (int j = 0; j < al.Length; j++)
                {
                    if (f == true)
                    {
                        if (s[i] == al[j])
                        {


                            code.Append(al[(j + step) % al.Length]);

                        }
                    }
                    else
                    {
                        if (s[i] == al[j])
                            code.Append(al[(j - step + al.Length) % al.Length]);
                    }
                }
            }


            string str = code.ToString();


            return str;


        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {

            string lang = "eng";

            f = false;

            foreach (char c in textBox1.Text)
            {
                if ((c > 'а' && c < 'я') || (c > 'А' && c < 'Я'))
                    lang = "ru";
                else if ((c > 'a' && c < 'z') || (c > 'A' && c < 'Z'))
                    lang = "eng";
            }


            //расшифровать
            if (str_File == "")

            {

                foreach (char c in str_File)
                {
                    if ((c > 'а' && c < 'я') || (c > 'А' && c < 'Я'))
                        lang = "ru";
                    else if ((c > 'a' && c < 'z') || (c > 'A' && c < 'Z'))
                        lang = "eng";
                }

                if (comboBox1.SelectedIndex == 0)
                {
                    textBox2.Text = Cezar(textBox1.Text, textBox3.Text, f);
                }


                if (comboBox1.SelectedIndex == 1)
                {
                    textBox2.Text = Decipher(textBox1.Text, textBox3.Text, lang); ;

                }



                if (comboBox1.SelectedIndex == 2)
                {

                    textBox2.Text = PlayfairDecryption(textBox1.Text.ToLower(), textBox3.Text.ToLower(), lang);

                }
            }
            else

            {
                foreach (char c in str_File)
                {
                    if ((c > 'а' && c < 'я') || (c > 'А' && c < 'Я'))
                        lang = "ru";
                    else if ((c > 'a' && c < 'z') || (c > 'A' && c < 'Z'))
                        lang = "eng";
                }




                if (comboBox1.SelectedIndex == 0)
                {
                    textBox2.Text = Cezar(str_File, textBox3.Text, f);

                }

                if (comboBox1.SelectedIndex == 1)
                {
                    textBox2.Text = Decipher(str_File, textBox3.Text, lang); ;

                }



                if (comboBox1.SelectedIndex == 2)
                {
                    textBox2.Text = PlayfairDecryption(str_File.ToLower(), textBox3.Text.ToLower(), lang);
                }
            }




        }

       

        private void button3_Click(object sender, EventArgs e)
        {
           
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }



        static string PlayfairEncryption(string sInput, string sKey, string lang)


        {

            string zam = "x";

            int m = 5;

            if (lang == "eng")
            {


                zam = "x";

                m = 5;
            }


            if (lang == "ru")
            {

                m = 6;
                zam = "ъ";


            }


            string sEncryptedText = string.Empty;


            if ((sKey != "") && (sInput != ""))


            {


                sKey = sKey.ToLower();


                string sGrid = null;


                string sAlpha = "abcdefghiklmnopqrstuvwxyz";


                if (lang == "eng")
                {


                    sAlpha = "abcdefghiklmnopqrstuvwxyz";


                }


                if (lang == "ru")
                {


                    sAlpha = "абвгдежзийклмнопрстуфхцчшщъыьэюя";


                }


                sInput = sInput.ToLower();


                string sOutput = "";


                Regex rgx = new Regex("[^a-z-]");



                if (lang == "eng")
                {


                    rgx = new Regex("[^a-z-]");


                }


                if (lang == "ru")
                {


                    rgx = new Regex("[^а-я-]");


                }



                sKey = rgx.Replace(sKey, "");





                sKey = sKey.Replace('j', 'i');



                if (lang == "ru")
                {


                    sKey = sKey.Replace('ё', 'е');


                }


                for (int i = 0; i < sKey.Length; i++)


                {


                    if ((sGrid == null) || (!sGrid.Contains(sKey[i])))


                    {


                        sGrid += sKey[i];


                    }


                }





                for (int i = 0; i < sAlpha.Length; i++)


                {


                    if (!sGrid.Contains(sAlpha[i]))


                    {


                        sGrid += sAlpha[i];


                    }


                }





                sInput = rgx.Replace(sInput, "");





                sInput = sInput.Replace('j', 'i');


                if (lang == "ru")
                {


                    sInput = sInput.Replace('ё', 'е');


                }



                for (int i = 0; i < sInput.Length; i += 2)


                {


                    if (((i + 1) < sInput.Length) && (sInput[i] == sInput[i + 1]))


                    {


                        sInput = sInput.Insert(i + 1, zam);


                    }


                }





                if ((sInput.Length % 2) > 0)


                {


                    sInput += zam;


                }





                int iTemp = 0;


                do


                {


                    int iPosA = sGrid.IndexOf(sInput[iTemp]);


                    int iPosB = sGrid.IndexOf(sInput[iTemp + 1]);


                    int iRowA = iPosA / 5;


                    int iColA = iPosA % 5;


                    int iRowB = iPosB / 5;


                    int iColB = iPosB % 5;





                    if (iColA == iColB)


                    {


                        iPosA += 5;


                        iPosB += 5;


                    }


                    else


                    {


                        if (iRowA == iRowB)


                        {


                            if (iColA == 4)


                            {


                                iPosA -= 4;


                            }


                            else


                            {


                                iPosA += 1;


                            }


                            if (iColB == 4)


                            {


                                iPosB -= 4;


                            }


                            else


                            {


                                iPosB += 1;


                            }


                        }


                        else


                        {


                            if (iRowA < iRowB)


                            {


                                iPosA -= iColA - iColB;


                                iPosB += iColA - iColB;


                            }


                            else


                            {


                                iPosA += iColB - iColA;


                                iPosB -= iColB - iColA;


                            }


                        }


                    }





                    if (iPosA >= sGrid.Length)


                    {


                        iPosA = 0 + (iPosA - sGrid.Length);


                    }





                    if (iPosB >= sGrid.Length)


                    {


                        iPosB = 0 + (iPosB - sGrid.Length);


                    }





                    if (iPosA < 0)


                    {


                        iPosA = sGrid.Length + iPosA;


                    }





                    if (iPosB < 0)


                    {


                        iPosB = sGrid.Length + iPosB;


                    }





                    sOutput += sGrid[iPosA].ToString() + sGrid[iPosB].ToString();





                    iTemp += 2;


                } while (iTemp < sInput.Length);





                sEncryptedText = sOutput;


            }


            return sEncryptedText;


        }





        static string PlayfairDecryption(string sCipherText, string sKey, string lang)


        {


            sKey = sKey.ToLower();


            string sGrid = null;


            string sAlpha = "abcdefghiklmnopqrstuvwxyz";


            string zam = "x";

            int m = 5;

            if (lang == "eng")
            {


                zam = "x";

                m = 5;
            }


            if (lang == "ru")
            {

                m = 6;
                zam = "ъ";


            }




            if (lang == "eng")
            {


                sAlpha = "abcdefghiklmnopqrstuvwxyz";


            }


            if (lang == "ru")
            {


                sAlpha = "абвгдежзийклмнопрстуфхцчшщъыьэюя";


            }




            string sInput = sCipherText.ToLower();


            string sOutput = "";





            sKey = sKey.Replace('j', 'i');


            if (lang == "ru")
            {


                sKey = sKey.Replace('ё', 'е');


            }



            for (int i = 0; i < sKey.Length; i++)


            {


                if ((sGrid == null) || (!sGrid.Contains(sKey[i])))


                {


                    sGrid += sKey[i];


                }


            }





            for (int i = 0; i < sAlpha.Length; i++)


            {


                if (!sGrid.Contains(sAlpha[i]))


                {


                    sGrid += sAlpha[i];


                }


            }





            int iTemp = 0;


            do


            {


                int iPosA = sGrid.IndexOf(sInput[iTemp]);


                int iPosB = sGrid.IndexOf(sInput[iTemp + 1]);


                int iRowA = iPosA / 5;


                int iColA = iPosA % 5;


                int iRowB = iPosB / 5;


                int iColB = iPosB % 5;





                if (iColA == iColB)


                {


                    iPosA -= 5;


                    iPosB -= 5;


                }


                else


                {


                    if (iRowA == iRowB)


                    {


                        if (iColA == 0)


                        {


                            iPosA += 4;


                        }


                        else


                        {


                            iPosA -= 1;


                        }


                        if (iColB == 0)


                        {


                            iPosB += 4;


                        }


                        else


                        {


                            iPosB -= 1;


                        }


                    }


                    else


                    {


                        if (iRowA < iRowB)


                        {


                            iPosA -= iColA - iColB;


                            iPosB += iColA - iColB;


                        }


                        else


                        {


                            iPosA += iColB - iColA;


                            iPosB -= iColB - iColA;


                        }


                    }


                }





                if (iPosA > sGrid.Length)


                {


                    iPosA = 0 + (iPosA - sGrid.Length);


                }





                if (iPosB > sGrid.Length)


                {


                    iPosB = 0 + (iPosB - sGrid.Length);


                }





                if (iPosA < 0)


                {


                    iPosA = sGrid.Length + iPosA;


                }





                if (iPosB < 0)


                {


                    iPosB = sGrid.Length + iPosB;


                }





                sOutput += sGrid[iPosA].ToString() + sGrid[iPosB].ToString();





                iTemp += 2;


            } while (iTemp < sInput.Length);





            return sOutput;


        }




        private static int Mod(int a, int b)
        {
            return (a % b + b) % b;
        }

        private static string Cipher(string input, string key, bool encipher, string lang)
        {
            int al;

            if (lang =="eng")
            {
                al = 26;
            }
            else
            {
                al = 32;
            }

            for (int i = 0; i < key.Length; ++i)
                if (!char.IsLetter(key[i]))
                    return null; 

            string output = string.Empty;
            int nonAlphaCharCount = 0;

            for (int i = 0; i < input.Length; ++i)
            {
                if (char.IsLetter(input[i]))
                {
                    bool cIsUpper = char.IsUpper(input[i]);
                    char offset = cIsUpper ? 'A' : 'a';
                    if (lang == "eng")
                    {
                         offset = cIsUpper ? 'A' : 'a'; ;
                    }
                    else
                    {
                         offset = cIsUpper ? 'А' : 'а'; ;
                    }
                    
                    int keyIndex = (i - nonAlphaCharCount) % key.Length;
                    int k = (cIsUpper ? char.ToUpper(key[keyIndex]) : char.ToLower(key[keyIndex])) - offset;
                    k = encipher ? k : -k;
                    char ch = (char)((Mod(((input[i] + k) - offset), al)) + offset);
                    output += ch;
                }
                else
                {
                    output += input[i];
                    ++nonAlphaCharCount;
                }
            }

            return output;
        }

        public static string Encipher(string input, string key,string lang)
        {
            return Cipher(input, key, true,lang);
        }

        public static string Decipher(string input, string key, string lang)
        {
            return Cipher(input, key, false, lang);
        }



    }
}
